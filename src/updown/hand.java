/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package updown;

/**
 *
 * @author DHAIRYA
 */
public class hand {
    private String colour;
    private boolean greater;
    private boolean lesser;

    public hand(String colour, boolean greater, boolean lesser) {
        this.colour = colour;
        this.greater = greater;
        this.lesser = lesser;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public boolean isGreater() {
        return greater;
    }

    public void setGreater(boolean greater) {
        this.greater = greater;
    }

    public boolean isLesser() {
        return lesser;
    }

    public void setLesser(boolean lesser) {
        this.lesser = lesser;
    }
    
    
}
