/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package updown;

/**
 *
 * @author DHAIRYA
 */
public class deck {
    private int no_of_card;
    private String Hand;
    private String colour;

    public deck(int no_of_card, String Hand, String colour) {
        this.no_of_card = no_of_card;
        this.Hand = Hand;
        this.colour = colour;
    }

    public int getNo_of_card() {
        return no_of_card;
    }

    public void setNo_of_card(int no_of_card) {
        this.no_of_card = no_of_card;
    }

    public String getHand() {
        return Hand;
    }

    public void setHand(String Hand) {
        this.Hand = Hand;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }
    
    
}
