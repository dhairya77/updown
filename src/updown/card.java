/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package updown;

/**
 *
 * @author DHAIRYA
 */
public class card {
    private String total;
    private double select_card;
    private String hand;

    public card(String total, double select_card, String hand) {
        this.total = total;
        this.select_card = select_card;
        this.hand = hand;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public double getSelect_card() {
        return select_card;
    }

    public void setSelect_card(double select_card) {
        this.select_card = select_card;
    }

    public String getHand() {
        return hand;
    }

    public void setHand(String hand) {
        this.hand = hand;
    }
    
    
}
