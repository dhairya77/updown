/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package updown;

/**
 *
 * @author DHAIRYA
 */
public class player {
    private String winner;
    private String looser;
    private String comp;
    private double count;

    public player(String winner, String looser, String comp, double count) {
        this.winner = winner;
        this.looser = looser;
        this.comp = comp;
        this.count = count;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getLooser() {
        return looser;
    }

    public void setLooser(String looser) {
        this.looser = looser;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }
    
    
}
